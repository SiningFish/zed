package com.origin.zed.service.user.dto;

import com.origin.zed.service.user.common.Constant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Result<T>{
    @Getter
    @Setter
    private int code = Constant.SUCCESS;

    @Getter
    @Setter
    private String msg = "success";

    @Getter
    @Setter
    private T data;

    public Result(){
        super();
    }

    public Result(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
    public Result(T data){
        this.data = data;
    }
    public Result(T data, String msg){
        this.data = data;
        this.msg = msg;
    }
    public Result(Throwable throwable){
        this.msg = throwable.getMessage();
        this.code = Constant.FAIL;
    }
}
