package com.origin.zed.service.user.service;

import com.origin.zed.service.user.domain.User;

public interface UserService {
    User selectById(Long id);
}
