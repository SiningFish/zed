package com.origin.zed.service.user.controller;

import com.origin.zed.service.user.common.Constant;
import com.origin.zed.service.user.common.Message;
import com.origin.zed.service.user.domain.User;
import com.origin.zed.service.user.dto.Result;
import com.origin.zed.service.user.dto.UserDto;
import com.origin.zed.service.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Value("${server.port}")
    private int port = 0;

    @GetMapping("/{id}")
    public Result getById(@PathVariable("id") Long id){
        User user = userService.selectById(id);
        return user!=null?new Result<>(new UserDto(user, port), Message.QUERY_SUCCESS):new Result<>(Constant.FAIL, Message.QUERY_FAILED);
    }
}
