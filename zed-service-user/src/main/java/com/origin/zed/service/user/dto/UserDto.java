package com.origin.zed.service.user.dto;

import com.origin.zed.service.user.domain.User;
import lombok.Data;

@Data
public class UserDto {
    private Integer port;
    private Long userId;

    private String userName;

    private String userPassword;
    public UserDto(User user, Integer port){
        this.port=port;
        this.userId=user.getUserId();
        this.userName=user.getUserName();
        this.userPassword=user.getUserPassword();
    }

}