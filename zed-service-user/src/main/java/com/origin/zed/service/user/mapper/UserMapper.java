package com.origin.zed.service.user.mapper;

import com.origin.zed.service.user.domain.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int deleteByPrimaryKey(Long userId);

    int insert(User record);

    int insertSelective(User record);

    User selectById(Long userId); //只用到了这个，其他的没用到
    User selectByIdAndPassword(@Param("id") Long userId, @Param("psd")String password);
    User selectByNameAndPassword(@Param("name")String name, @Param("psd")String password);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}