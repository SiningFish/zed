package com.origin.zed.service.user.service.impl;

import com.origin.zed.service.user.domain.User;
import com.origin.zed.service.user.mapper.UserMapper;
import com.origin.zed.service.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

//    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectById(Long id) {
        return userMapper.selectById(id);
    }
}
