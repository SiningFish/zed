/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : zed

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 19/03/2019 14:46:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for zed_role
-- ----------------------------
DROP TABLE IF EXISTS `zed_role`;
CREATE TABLE `zed_role`  (
  `role_id` bigint(20) NOT NULL COMMENT '权限ID',
  `role_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of zed_role
-- ----------------------------
INSERT INTO `zed_role` VALUES (1, '管理员');
INSERT INTO `zed_role` VALUES (2, '普通用户');

-- ----------------------------
-- Table structure for zed_user
-- ----------------------------
DROP TABLE IF EXISTS `zed_user`;
CREATE TABLE `zed_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户主键',
  `user_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户姓名',
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of zed_user
-- ----------------------------
INSERT INTO `zed_user` VALUES (1, 'admin', 'admin');
INSERT INTO `zed_user` VALUES (2, 'test', 'test');
INSERT INTO `zed_user` VALUES (3, 'dingtao', 'dingtao');

-- ----------------------------
-- Table structure for zed_user_role
-- ----------------------------
DROP TABLE IF EXISTS `zed_user_role`;
CREATE TABLE `zed_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT ' 用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of zed_user_role
-- ----------------------------
INSERT INTO `zed_user_role` VALUES (1, 1);
INSERT INTO `zed_user_role` VALUES (2, 2);
INSERT INTO `zed_user_role` VALUES (3, 1);

SET FOREIGN_KEY_CHECKS = 1;
