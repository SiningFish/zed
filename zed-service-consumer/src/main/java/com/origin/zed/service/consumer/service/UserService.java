package com.origin.zed.service.consumer.service;

import com.origin.zed.service.consumer.dto.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("ZED-SERVICE-USER")
public interface UserService {
    @GetMapping("/user/{id}")
    public Result getById(@PathVariable("id") Long id);
}
