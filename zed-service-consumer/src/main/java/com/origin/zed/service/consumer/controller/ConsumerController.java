package com.origin.zed.service.consumer.controller;

import com.origin.zed.service.consumer.dto.Result;
import com.origin.zed.service.consumer.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer")
@AllArgsConstructor
public class ConsumerController {

    private UserService userService;

    @GetMapping("/user/{id}")
    public Result getById(@PathVariable("id") Long id){
        return userService.getById(id);
    }
}
