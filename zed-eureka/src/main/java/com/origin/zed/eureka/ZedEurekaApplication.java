package com.origin.zed.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class ZedEurekaApplication {
    public static void main(String args[]){
        SpringApplication.run(ZedEurekaApplication.class, args);
    }
}
